package com.example.ProjetAndroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class Accueil extends Activity {
    /**
     * Called when the activity is first created.
     */

    private BDD bdd;

    List<String> values;
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        bdd = new BDD(this);

        List<String> list = bdd.getValues();

        ListView lv=(ListView)findViewById(R.id.listView);
        values = new ArrayList<String>();

        for (int i=0; i<list.size();i++){
            values.add(list.get(i));
        }

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,values);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Accueil.this, "Vous avez cliquez sur " + position, Toast.LENGTH_SHORT).show();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Accueil.this, "Vous avez cliquez LONGUEMENT sur " + position, Toast.LENGTH_SHORT).show();
                delItem(position);
                return true;
            }
        });
    }

    public void delItem(int pos){
        values.remove(pos);
        adapter.notifyDataSetChanged();
        bdd.deleteValue(values.get(pos));
    }

    public void addItem(){
        adapter.add("valeur "+bdd.getValues().size());
        bdd.insertValue("valeur "+bdd.getValues().size());
    }

    public void click_button(View v){
        addItem();
    }
}
