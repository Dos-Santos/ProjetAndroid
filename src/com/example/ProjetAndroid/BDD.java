package com.example.ProjetAndroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dossantos on 09/03/16.
 */
public class BDD extends SQLiteOpenHelper {

    public static final String KEY_ID = "_id";
    public static final String TITRE = "titre";
    public static final String TEXTE = "texte";

    private static final String DB_NAME = "BDD";
    private static final String DB_TABLE_NAME = "valeurs";

    private SQLiteDatabase bdd;

    BDD(Context context){
        super(context,DB_NAME,null,2);
        bdd = getWritableDatabase();
    }

    /*methode dans le cas ou la db n existe pas*/
    @Override
    public void onCreate(SQLiteDatabase bdd){
        bdd.execSQL("create table "+ DB_TABLE_NAME +"("+KEY_ID+" integer primary key autoincrement," +
                " "+TITRE+" text not null," +
                " "+TEXTE+" text)");
    }

    /*methode pour changement de version sqlite*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion){

    }

    /*insertion des données*/
    public void insertValue(String value){
        //pour insertion, il faut utiliser un content value
        ContentValues content = new ContentValues();
        content.put("value",value);
        bdd.insert(DB_TABLE_NAME, null,content);
    }

    public void deleteValue(String value){
        bdd.delete(DB_TABLE_NAME, KEY_ID + " = ?", new String[] {String.valueOf(value)});
    }

    /*recuperation des données*/
    public List<String> getValues(){
        List<String> list=new ArrayList<String>();
        String[] columns={"value"};
        Cursor cursor = bdd.query(DB_TABLE_NAME,columns,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
}
